﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//
// Summary:
//     ///
//     Representation of 2D vectors and points.
//     ///
public struct IntVect3
{
    //
    // Summary:
    //     ///
    //     X component of the vector.
    //     ///
    public int x;

    //
    // Summary:
    //     ///
    //     Y component of the vector.
    //     ///
    public int y;

    //
    // Summary:
    //     ///
    //     Z component of the vector.
    //     ///
    public int z;

    //
    // Summary:
    //     ///
    //     Constructs a new vector with given x, y, z components.
    //     ///
    //
    // Parameters:
    //   x:
    //
    //   y:
    public IntVect3(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //
    // Summary:
    //     ///
    //     Shorthand for writing IntVect3(0, 0, 0).
    //     ///
    public static IntVect3 zero { get { return new IntVect3(0, 0, 0); } }

    //
    // Summary:
    //     ///
    //     Returns a vector that is made from the largest components of two vectors.
    //     ///
    //
    // Parameters:
    //   lhs:
    //
    //   rhs:
    public static IntVect3 Max(IntVect3 lhs, IntVect3 rhs)
    {
        return new IntVect3(
            Mathf.Max(lhs.x, rhs.x),
            Mathf.Max(lhs.y, rhs.y),
            Mathf.Max(lhs.z, rhs.z)
            );
    }

    //
    // Summary:
    //     ///
    //     Returns a vector that is made from the smallest components of two vectors.
    //     ///
    //
    // Parameters:
    //   lhs:
    //
    //   rhs:
    public static IntVect3 Min(IntVect3 lhs, IntVect3 rhs)
    {
        return new IntVect3(
            Mathf.Min(lhs.x, rhs.x),
            Mathf.Min(lhs.y, rhs.y),
            Mathf.Min(lhs.z, rhs.z)
            );
    }

    //
    // Summary:
    //     ///
    //     Set x, y and z components of an existing IntVect3.
    //     ///
    //
    // Parameters:
    //   new_x:
    //
    //   new_y:
    //
    //   new_z:
    public void Set(int new_x, int new_y, int new_z)
    {
        this.x = new_x;
        this.y = new_y;
        this.z = new_z;
    }

    //
    // Summary:
    //     ///
    //     Returns a nicely formatted string for this vector.
    //     ///
    //
    // Parameters:
    //   format:
    public override string ToString()
    {
        return "(" + x.ToString() + ", " + y.ToString() + ", " + z.ToString() + ")";
    }

    public override bool Equals(object obj)
    {
        //Check for null and compare run-time types
        if ((obj == null) || !this.GetType().Equals(obj.GetType()))
        {
            return false;
        }
        else
        {
            IntVect3 p = (IntVect3) obj;
            return (x == p.x) && (y == p.y) && (z == p.z);
        }
    }

    public override int GetHashCode()
    {
        return (x | y) << 2 ^ z;
    }

    public static IntVect3 operator +(IntVect3 a, IntVect3 b)
    {
        return new IntVect3(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static IntVect3 operator -(IntVect3 a)
    {
        return new IntVect3(-a.x, -a.y, -a.z);
    }

    public static IntVect3 operator -(IntVect3 a, IntVect3 b)
    {
        return new IntVect3(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    public static IntVect3 operator *(int d, IntVect3 a)
    {
        return new IntVect3(a.x * d, a.y * d, a.z * d);
    }

    public static IntVect3 operator *(IntVect3 a, int d)
    {
        return new IntVect3(a.x * d, a.y * d, a.z * d);
    }

    public static IntVect3 operator /(IntVect3 a, int d)
    {
        return new IntVect3(a.x / d, a.y / d, a.z / d);
    }

    public static bool operator ==(IntVect3 lhs, IntVect3 rhs)
    {
        return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
    }

    public static bool operator !=(IntVect3 lhs, IntVect3 rhs)
    {
        return lhs.x != rhs.x || lhs.y != rhs.y || lhs.z != rhs.z;
    }

    public static implicit operator IntVect3(IntVect2 v)
    {
        return new IntVect3(v.x, v.y, 0);
    }

    public static implicit operator IntVect3(Vector2 v)
    {
        return new IntVect3((int)v.x, (int)v.y, 0);
    }

    public static implicit operator Vector2(IntVect3 v)
    {
        return new Vector2(v.x, v.y);
    }

    public static implicit operator IntVect3(Vector3 v)
    {
        return new IntVect3((int)v.x, (int)v.y, (int)v.z);
    }

    public static implicit operator Vector3(IntVect3 v)
    {
        return new Vector3(v.x, v.y, v.z);
    }
}
