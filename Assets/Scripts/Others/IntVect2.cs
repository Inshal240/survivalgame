﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// TODO: Add 'row' and 'col' getters and setters

//
// Summary:
//     ///
//     Representation of 2D vectors and points.
//     ///
public struct IntVect2
{
    //
    // Summary:
    //     ///
    //     X component of the vector.
    //     ///
    public int x;

    public int row
    {
        get { return x; }
        set { x = value; }
    }
    
    //
    // Summary:
    //     ///
    //     Y component of the vector.
    //     ///
    public int y;

    public int col
    {
        get { return y; }
        set { y = value; }
    }

    //
    // Summary:
    //     ///
    //     Constructs a new vector with given x, y components.
    //     ///
    //
    // Parameters:
    //   x:
    //
    //   y:
    public IntVect2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    //
    // Summary:
    //     ///
    //     Shorthand for writing IntVect2(0, 0).
    //     ///
    public static IntVect2 zero { get { return new IntVect2(0, 0); }  }

    //
    // Summary:
    //     ///
    //     Returns a vector that is made from the largest components of two vectors.
    //     ///
    //
    // Parameters:
    //   lhs:
    //
    //   rhs:
    public static IntVect2 Max(IntVect2 lhs, IntVect2 rhs)
    {
        return new IntVect2(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y));
    }
    
    //
    // Summary:
    //     ///
    //     Returns a vector that is made from the smallest components of two vectors.
    //     ///
    //
    // Parameters:
    //   lhs:
    //
    //   rhs:
    public static IntVect2 Min(IntVect2 lhs, IntVect2 rhs)
    {
        return new IntVect2(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y));
    }

    //
    // Summary:
    //     ///
    //     Set x and y components of an existing IntVect2.
    //     ///
    //
    // Parameters:
    //   new_x:
    //
    //   new_y:
    public void Set(int new_x, int new_y)
    {
        this.x = new_x;
        this.y = new_y;
    }

    //
    // Summary:
    //     ///
    //     Returns a nicely formatted string for this vector.
    //     ///
    //
    // Parameters:
    //   format:
    public override string ToString()
    {
        return "(" + x.ToString() + ", " + y.ToString() + ")";
    }

    public override bool Equals(object obj)
    {
        //Check for null and compare run-time types
        if ((obj == null) || !this.GetType().Equals(obj.GetType()))
        {
            return false;
        }
        else
        {
            IntVect2 p = (IntVect2)obj;
            return (x == p.x) && (y == p.y);
        }
    }

    public override int GetHashCode()
    {
        return x << 2 ^ y;
    }

    public static IntVect2 operator +(IntVect2 a, IntVect2 b)
    {
        return new IntVect2(a.x + b.x, a.y + b.y);
    }

    public static IntVect2 operator -(IntVect2 a)
    {
        return new IntVect2(-a.x, -a.y);
    }

    public static IntVect2 operator -(IntVect2 a, IntVect2 b)
    {
        return new IntVect2(a.x - b.x, a.y - b.y);
    }

    public static IntVect2 operator *(int d, IntVect2 a)
    {
        return new IntVect2(a.x * d, a.y * d);
    }

    public static IntVect2 operator *(IntVect2 a, int d)
    {
        return new IntVect2(a.x * d, a.y * d);
    }

    public static IntVect2 operator /(IntVect2 a, int d)
    {
        return new IntVect2(a.x / d, a.y / d);
    }

    public static bool operator ==(IntVect2 lhs, IntVect2 rhs)
    {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }

    public static bool operator !=(IntVect2 lhs, IntVect2 rhs)
    {
        return lhs.x != rhs.x || lhs.y != rhs.y;
    }

    public static implicit operator IntVect2(IntVect3 v)
    {
        return new IntVect2(v.x, v.y);
    }

    public static implicit operator IntVect2(Vector2 v)
    {
        return new IntVect2((int)v.x, (int)v.y);
    }

    public static implicit operator Vector2(IntVect2 v)
    {
        return new Vector2(v.x, v.y);
    }

    public static implicit operator IntVect2(Vector3 v)
    {
        return new IntVect2((int)v.x, (int)v.y);
    }

    public static implicit operator Vector3(IntVect2 v)
    {
        return new Vector3(v.x, v.y);
    }
}
