﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// TODO:
interface IConsumable
{
    // This should return info about the object
    //ConsumableInfo GetConsumableInfo();
    
    // This should destroy the object and return base components if any
    // Refers to eating or drinking
    GameItem[] UseAsConsumable();
}