﻿using System;
using UnityEngine;

// TODO: Add weapon related functions such as equip
interface IWeapon
{
    WeaponInfo GetWeaponInfo();
}
