﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// TODO:
// IDamageable or IWearsOverTime should be implemented before this interface
interface IRepairable
{
    // Should repair the item health and "use" the argument and return the remainder
    GameItem[] Repair(IConsumable mItem);
}