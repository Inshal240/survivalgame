﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshRenderer))]
public class MeshRendererOrder : MonoBehaviour
{
    public string sortingLayer = "Default";
    public int orderInLayer = 0;

    private void Awake()
    {
        var rend = GetComponent<MeshRenderer>();

        rend.sortingLayerID = SortingLayer.NameToID(sortingLayer);
        rend.sortingOrder = orderInLayer;
    }

    #if UNITY_EDITOR
    private void OnValidate()
    {
        var rend = GetComponent<MeshRenderer>();

        rend.sortingLayerID = SortingLayer.NameToID(sortingLayer);
        rend.sortingOrder = orderInLayer;

        Debug.Log("Sorting layer changed to " + rend.sortingLayerName, this);
    }
    #endif
}
