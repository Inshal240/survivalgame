using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(GridLayoutGroup))]
public class CellResize : MonoBehaviour
{
    void OnEnable()
    {
        ResizeCells();
    }
    
    void OnValidate()
    {
        ResizeCells();
    }

    void ResizeCells()
    {
        GridLayoutGroup grid = GetComponent<GridLayoutGroup>();
        float width = GetComponent<RectTransform>().rect.width;
        grid.cellSize = new Vector2(width, width) / grid.constraintCount;
    }
}
