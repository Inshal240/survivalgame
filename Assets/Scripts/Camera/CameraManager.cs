#define DEBUG_CAMERA

using System.Collections;
using UnityEngine;
using UnityEditor;


public class CameraManager : MonoBehaviour
{
    public float edgeScrollDistance = 10;       // Distance from the edge to activate mouse scrolling
    public float panSpeed = 20;
    public float zoomSpeed = 3f;
    public float minZoom = 1f;
    public float maxZoom = 15f;

    public bool mouseScrollEnable = false;
    public bool keyboardScrollEnable = true;

#if DEBUG_CAMERA
    public bool showMapArea = true;
    public Color mapAreaColor = Color.red;
    public bool showFocus = true;
    public Color cameraFocusColor = Color.green;
#endif

    // Mouse Positions
    private float mousePosX;
    private float mousePosY;

    // Camera Positions
    private float cameraFocusPosX;
    private float cameraFocusPosZ;

    // Extreme edges of the map
    [SerializeField]
    private Rect mapArea;

    // Update is called once per frame
    void Update()
    {
        cameraFocusPosX = transform.position.x;
        cameraFocusPosZ = transform.position.z;

        // Camera movement by mouse position
        if (mouseScrollEnable)
        {
            MoveCameraUsingMouse();
        }

        // Camera movement by keyboard
        if (keyboardScrollEnable)
        {
            MoveCameraUsingKeyboard();
        }

        // Zoom in and out using mouse wheel
        if (Camera.main.orthographicSize < maxZoom && Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            Camera.main.orthographicSize += zoomSpeed * Time.deltaTime;
        }

        if (Camera.main.orthographicSize > minZoom && Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            Camera.main.orthographicSize -= zoomSpeed * Time.deltaTime;
        }
    }

#if DEBUG_CAMERA
    public void OnDrawGizmos()
    {
        if (showMapArea)
        {
            var mapCenter = new Vector3(mapArea.x + mapArea.width, 0, mapArea.y + mapArea.height) * 0.5f;
            var mapSize = new Vector3(mapArea.width, 0, mapArea.height);

            Gizmos.color = mapAreaColor;
            Gizmos.DrawCube(mapCenter, mapSize);
        }

        if (showFocus)
        {
            Gizmos.color = cameraFocusColor;
            Gizmos.DrawSphere(transform.position, 0.5f);
        }
    }
#endif

    /// <summary>
    /// Updates the viewable map area.
    /// </summary>
    /// <param name="mapArea"></param>
    public void SetMapArea(Rect mapArea)
    {
        this.mapArea = mapArea;
    }

    /// <summary>
    /// Moves camera to the center of the map.
    /// </summary>
    public void MoveCameraToCenter()
    {
        var board = BoardManager.Singleton;

        // Gets the center tile and places the camera on top and to the left a bit
        // Moving to the left is required because the tiles in the first column are
        // split by the y = 0 world coordinate line. Same for x.
        transform.position =
            board.HexGrid[board.GridCols / 2][board.GridRows / 2].HexCenter;

    }

    /// <summary>
    /// Moves the camera to focus at a point in world space.
    /// </summary>
    /// <param name="point"></param>
    public void MoveCameraToPoint(Vector3 point)
    {
        transform.position = point;
    }

    // Helper Functions
    private void PanCameraInDirection(Vector3 direction)
    {
        transform.Translate(direction * panSpeed * Time.deltaTime, Space.World);
    }
    
    /// <summary>
    /// Moves the camera using mouse position.
    /// </summary>
    private void MoveCameraUsingMouse()
    {
        mousePosX = Input.mousePosition.x;
        mousePosY = Input.mousePosition.y;

        // Left
        if (mousePosX < edgeScrollDistance && cameraFocusPosX > mapArea.x)
        {
            PanCameraInDirection(Vector3.left);
        }

        // Right
        if (mousePosX >= Screen.width - edgeScrollDistance && cameraFocusPosX < (mapArea.x + mapArea.width))
        {
            PanCameraInDirection(Vector3.right);
        }

        // Up
        if (mousePosY >= Screen.height - edgeScrollDistance && cameraFocusPosZ < (mapArea.y + mapArea.height))
        {
            PanCameraInDirection(Vector3.forward);
        }

        // Down
        if (mousePosY < edgeScrollDistance && cameraFocusPosZ > mapArea.y)
        {
            PanCameraInDirection(Vector3.back);
        }
    }

    /// <summary>
    /// Moves camera using WASD or cursor keys.
    /// </summary>
    private void MoveCameraUsingKeyboard()
    {
        // Left
        if (Input.GetAxis("Horizontal") < 0 && cameraFocusPosX > mapArea.x)
        {
            PanCameraInDirection(Vector3.left);
        }

        // Right
        if (Input.GetAxis("Horizontal") > 0 && cameraFocusPosX < (mapArea.x + mapArea.width))
        {
            PanCameraInDirection(Vector3.right);
        }

        // Up
        if (Input.GetAxis("Vertical") > 0 && cameraFocusPosZ < (mapArea.y + mapArea.height))
        {
            PanCameraInDirection(Vector3.forward);
        }

        // Down
        if (Input.GetAxis("Vertical") < 0 && cameraFocusPosZ > mapArea.y)
        {
            PanCameraInDirection(Vector3.back);
        }
    }
}
