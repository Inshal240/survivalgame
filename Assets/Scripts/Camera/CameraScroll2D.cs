﻿#if CAMERA_2D
using UnityEngine;
using System.Collections;

public class CameraScroll2D : MonoBehaviour
{
    [Tooltip("Offset at which the camera should be placed. It is assumed that origin is centered in the view.")]
    public Vector3 CameraOffset;

    public int ScrollArea;

    public float ScrollSpeed = 0.5f;
    public float ZoomSpeed = 50f;
    public float MinZoom = 1f;
    public float MaxZoom = 10f;

    public bool MouseScrollEnable = false;
    public bool KeyboardScrollEnable = true;


    // Mouse Positions
    private float mousePosX;
    private float mousePosY;

    // Camera Positions
    private float cameraPosX;
    private float cameraPosY;

    private float leftMaxDist;
    private float rightMaxDist;
    private float topMaxDist;
    private float bottomMaxDist;


    void Start()
    {
        Cursor.visible = true;

        // TODO: Create a visible target on y=0 to debug this
        // Max distance the camera can move in Unity space
        leftMaxDist = 0.75f * HexTile.HexSize.x * 2 + CameraOffset.x * 0.5f;
        rightMaxDist = 0.75f * HexTile.HexSize.x * (BoardManager.GridCols) + CameraOffset.x;
        topMaxDist = HexTile.HexSize.y * (BoardManager.GridRows - 4) + CameraOffset.y * 0.5f;
        bottomMaxDist = CameraOffset.y;

        // Gets the center tile and places the camera on top and to the left a bit
        // Moving to the left is required because the tiles in the first column are
        // split by the y = 0 world coordinate line. Same for x.
        transform.position = new Vector3(
            BoardManager.HexGrid[BoardManager.GridCols / 2][BoardManager.GridRows / 2].HexCenter.x - HexTile.HexSize.x / 2,
            BoardManager.HexGrid[BoardManager.GridCols / 2][BoardManager.GridRows / 2].HexCenter.y - HexTile.HexSize.y / 2,
            0)
            + CameraOffset;
    }

    // Update is called once per frame
    void Update()
    {
        cameraPosX = transform.position.x;
        cameraPosY = transform.position.y;

        // Camera movement by mouse position
        if (MouseScrollEnable)
        {
            moveCameraUsingMouse();
        }

        // Camera movement by keyboard
        if (KeyboardScrollEnable)
        {
            moveCameraUsingKeyboard();
        }

        // Zoom in and out using mouse wheel
        if (Camera.main.orthographicSize < MaxZoom && Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            Camera.main.orthographicSize += ZoomSpeed * Time.deltaTime;
        }

        if (Camera.main.orthographicSize > MinZoom && Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            Camera.main.orthographicSize -= ZoomSpeed * Time.deltaTime;
        }
    }

    // Helper Functions
    private void moveCamera(Vector3 direction)
    {
        transform.Translate(direction * ScrollSpeed * Time.deltaTime, Space.World);
    }
    
    /// <summary>
    /// Moves the camera using mouse position.
    /// </summary>
    private void moveCameraUsingMouse()
    {
        mousePosX = Input.mousePosition.x;
        mousePosY = Input.mousePosition.y;

        // Left
        if (mousePosX < ScrollArea && cameraPosX > leftMaxDist)
        {
            moveCamera(Vector3.left);
        }

        // Right
        if (mousePosX >= Screen.width - ScrollArea && cameraPosX < rightMaxDist)
        {
            moveCamera(Vector3.right);
        }

        // Up
        if (mousePosY >= Screen.height - ScrollArea && cameraPosY < topMaxDist)
        {
            moveCamera(Vector3.up);
        }

        // Down
        if (mousePosY < ScrollArea && cameraPosY > bottomMaxDist)
        {
            moveCamera(Vector3.down);
        }
    }

    /// <summary>
    /// Moves camera using WASD or cursor keys.
    /// </summary>
    private void moveCameraUsingKeyboard()
    {
        // Left
        if (Input.GetAxis("Horizontal") < 0 && cameraPosX > leftMaxDist)
        {
            moveCamera(Vector3.left);
        }

        // Right
        if (Input.GetAxis("Horizontal") > 0 && cameraPosX < rightMaxDist)
        {
            moveCamera(Vector3.right);
        }

        // Up
        if (Input.GetAxis("Vertical") > 0 && cameraPosY < topMaxDist)
        {
            moveCamera(Vector3.up);
        }

        // Down
        if (Input.GetAxis("Vertical") < 0 && cameraPosY > bottomMaxDist)
        {
            moveCamera(Vector3.down);
        }
    }
}
#endif