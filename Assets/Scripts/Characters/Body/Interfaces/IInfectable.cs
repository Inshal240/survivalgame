﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// TODO:
interface IInfectable
{
    void Infect(/**/);
    void WorsenInfection();
    void AlleviateInfection();
    void CureInfection();
}