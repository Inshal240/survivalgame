﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

interface IBleeds
{
    // Returns bleeding amount
    int Bleed();
}