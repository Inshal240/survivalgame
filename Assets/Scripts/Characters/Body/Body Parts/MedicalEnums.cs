﻿using System;

// BodyParts

[Flags]
public enum BruisedState
{
    None,
    Light,
    Heavy
}

[Flags]
public enum CutState
{
    None,
    Shallow,
    Deep
};

[Flags]
public enum BleedingState
{
    None,
    Light,
    Moderate,
    Profuse
};

[Flags]
public enum InfectionState
{
    None,
    Stage_1,
    Stage_2,
    Stage_3,
    Permanent
};

//TODO:
//[Flags]
//public enum WoundCleanState
//{
//
//};