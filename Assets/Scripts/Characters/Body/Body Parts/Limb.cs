﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// TODO:
class Limb : BodyPart, ISplintable
{
    protected bool isCrippled = false;

    public bool IsCrippled { get { return isCrippled; } }

    // Call base class' constructor
    public Limb(string mPartName) : base(mPartName) { }

    // TODO:
    public virtual void ApplySplint()
    {
        throw new NotImplementedException();
    }

    // TODO:
    public virtual void RemoveSplint()
    {
        throw new NotImplementedException();
    }
}
