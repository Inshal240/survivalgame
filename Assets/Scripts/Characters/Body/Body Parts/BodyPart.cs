﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// TODO: Implement class
 abstract class BodyPart : IBandageable, ISuturable, ICleanable, IInfectable, IHealsOverTime, IDamageable, IBleeds
{
    // Name of the body part
    protected readonly string partName;

    // Overall health of this part
    protected int health;
    
    // These hold the turn on which the injury/infection occur
    protected int damageTime = 0;
    protected int infectionTime = 0;

    protected bool isBruised = false;
    protected bool isBleeding = false;
    protected bool isBandaged = false;
    protected bool isInfected = false;
    protected bool isDisabled = false;

    protected BruisedState bruisedState = BruisedState.None;
    protected CutState cutState = CutState.None;
    protected BleedingState bleedingState = BleedingState.None;
    protected InfectionState infectionState = InfectionState.None;
    
    // TODO:
    //protected WoundCleanState wouldCleanState;

    //TODO: Check if this implementation works
    public const int MaxHealth = 100;
    
    public int Health { get { return health; } }

    public bool IsInfected  { get { return isInfected; } }
    public bool IsBruised   { get { return isBruised; } }
    public bool IsBleeding  { get { return isBleeding; } }
    public bool IsBandaged  { get { return isBandaged; } }
    public bool IsDisabled  { get { return isDisabled; } }

    public BruisedState BruisedState    { get { return bruisedState; } }
    public CutState CutState            { get { return cutState; } }
    public BleedingState BleedingState  { get { return bleedingState; } }
    public InfectionState InfectionState { get { return infectionState; } }
    //public WoundCleanState WouldCleanState;

    public BodyPart(string mPartName)
    {
        partName = mPartName;
    }

    // TODO: Add damage type and injuries on its basis
    public virtual void TakeDamage(int mAmount)
    {
        health = Math.Max(0, health - mAmount);

        // Set booleans based on damage type
        throw new NotImplementedException();
    }

    // NOTE: This needs to be called every turn
    // TODO:
    public virtual void UpdateDamage()
    {
        // Check if still bleeding

        // check whether infected
        throw new NotImplementedException();
    }

    // TODO:
    public virtual int Bleed()
    {
        throw new NotImplementedException();
    }

    // TODO: Only allow bandaging if injured
    public virtual void ApplyBandage(/* bandage */)
    {
        isBandaged = true;

        // update healing factor
        throw new NotImplementedException();
    }

    // TODO:
    public virtual void DirtyBandages()
    {
        throw new NotImplementedException();
    }

    // TODO:
    public virtual void RemoveBandage()
    {
        isBandaged = false;

        // update healing factor
        throw new NotImplementedException();
    }

    // TODO:
    public void ApplyAntiseptic()
    {
        throw new NotImplementedException();
    }

    // TODO:
    public virtual void ApplySutures()
    {
        throw new NotImplementedException();
    }

    // TODO:
    public virtual void DirtySutures()
    {
        throw new NotImplementedException();
    }

    // TODO:
    public virtual void RemoveSutures()
    {
        throw new NotImplementedException();
    }

    // TODO:
    public void Infect()
    {
        throw new NotImplementedException();
    }

    // TODO:
    public void WorsenInfection()
    {
        throw new NotImplementedException();
    }

    // TODO:
    public void AlleviateInfection()
    {
        throw new NotImplementedException();
    }

    // TODO:
    public void CureInfection()
    {
        throw new NotImplementedException();
    }
}