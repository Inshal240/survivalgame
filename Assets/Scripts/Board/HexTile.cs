﻿//#define TILES_2D

using UnityEngine;
using System.Collections;

public class HexTile : MonoBehaviour
{
    // Visibility variables
    private bool visible;

    // Sprite Renderers for texture changes
    private SpriteRenderer[] spriteRenderers;

    // Holds move costs for all tiles
    static private int[] moveCosts =
    {
        0, 1, 1, 1, 2, 3
    };

    // Position of hex in grid 
    public GridPosition Position;

    // Center of hex in Unity worldspace
    public Vector3 HexCenter;

    public HexType HexType;

    // Gets the movement cost for the current tile
    public int MovementCost { get { return moveCosts[(int)HexType]; } }

    // Used to save the radius of the hexagon calcuated from the sprite
#if TILES_2D
    static public Vector2 HexSize;
#else
    static public Vector3 HexSize;
#endif

    // Used for index the sprite renderers attached to a hex tile
    private enum SpriteRenderers
    {
#if TILES_2D
        HexTerrain,
#endif
        MovementOverlay,
        MouseOverlay,
        FogOfWarOverlay
    };

    // Use this for initialization
    void Start()
    {
#if TILES_2D
        visible = spriteRenderers[(int)SpriteRenderers.HexTerrain].enabled;
#else
        visible = GetComponent<MeshRenderer>().enabled;
#endif
    }

    void Awake()
    {
        if (HexCenter == Vector3.zero)
        {
            // After initialization of the polygoncollider set the values of HexSize for later calculations
#if TILES_2D
            HexSize = GetComponent<PolygonCollider2D>().bounds.size;
#else
            HexSize = GetComponent<MeshCollider>().bounds.size;
#endif
        }

        // This gets a list of all sprite renderers in the tile prefab. They are in the same order as
        // the game objects in the editor
        spriteRenderers = GetComponentsInChildren<SpriteRenderer>(true);
    }

    void OnMouseEnter()
    {
        SetMouseOverlay(HexMouseOverlay.Highlighted);
    }

    void OnMouseExit()
    {
        SetMouseOverlay(HexMouseOverlay.None);
    }

    public void SetVisibilty(HexVisibility mVisibility)
    {
        switch (mVisibility)
        {
            // Hex is not visible so disable all sprite renderer
            case HexVisibility.Invisible:
#if TILES_2D
                spriteRenderers[(int)SpriteRenderers.HexTerrain].enabled = false;
#else
                GetComponent<MeshRenderer>().enabled = false;
#endif

                spriteRenderers[(int)SpriteRenderers.MouseOverlay].gameObject.SetActive(false);
                spriteRenderers[(int)SpriteRenderers.MovementOverlay].gameObject.SetActive(false);
                spriteRenderers[(int)SpriteRenderers.FogOfWarOverlay].gameObject.SetActive(false);
                visible = false;
                break;

            // Add gray tint if the area has been explored but isn't visible at the moment
            case HexVisibility.Explored:
#if TILES_2D
                spriteRenderers[(int)SpriteRenderers.HexTerrain].enabled = true;
#else
                GetComponent<MeshRenderer>().enabled = true;
#endif

                spriteRenderers[(int)SpriteRenderers.FogOfWarOverlay].gameObject.SetActive(true);
                visible = true;
                break;

            // Illuminate the hex since it is in sight of the player
            case HexVisibility.Visible:
#if TILES_2D
                spriteRenderers[(int)SpriteRenderers.HexTerrain].enabled = true;
#else
                GetComponent<MeshRenderer>().enabled = true;
#endif
                spriteRenderers[(int)SpriteRenderers.FogOfWarOverlay].gameObject.SetActive(false);
                visible = true;
                break;

            default:
                Debug.Log("Invalid value for HexVisibility");
                break;
        }
    }

    // TODO: Change textures on the basis of mouse hover, mouse select and move options
    public void SetMovementOverlay(HexMovementOverlay mOverlay)
    {
        // If the hex is not visible then overlays aren't possible
        if (!visible) return;

        switch (mOverlay)
        {
            // If no overlasy is required then disable the entire Game Object entirely
            case HexMovementOverlay.None:
                spriteRenderers[(int)SpriteRenderers.MovementOverlay].gameObject.SetActive(false);
                break;
            
            // Enable game object and set it to the 'Move' colour
            case HexMovementOverlay.Move:
                spriteRenderers[(int)SpriteRenderers.MovementOverlay].gameObject.SetActive(true);
                // TODO: Find appropriate colour and replace placeholder
                spriteRenderers[(int)SpriteRenderers.MovementOverlay].color = Color.yellow;
                break;
            
            // Enable the game object and set it to the ''
            case HexMovementOverlay.Enemy:
                spriteRenderers[(int)SpriteRenderers.MovementOverlay].gameObject.SetActive(true);
                // TODO: Find appropriate colour and replace placeholder
                spriteRenderers[(int)SpriteRenderers.MovementOverlay].color = Color.red;
                break;

            default:
                Debug.Log("Invalid value for MovementOverlay");
                break;
        }
    }

    public void SetMouseOverlay(HexMouseOverlay mMouseOverlay)
    {
        // If the hex is not visible then overlays aren't possible
        if (!visible) return;

        switch (mMouseOverlay)
        {
            case HexMouseOverlay.None:
                spriteRenderers[(int)SpriteRenderers.MouseOverlay].gameObject.SetActive(false);
                break;

            case HexMouseOverlay.Highlighted:
                spriteRenderers[(int)SpriteRenderers.MouseOverlay].gameObject.SetActive(true);
                break;

            default:
                Debug.Log("Invalid value for MouseOverlay");
                break;
        }
    }

    public void SetTexture(Sprite mSprite)
    {
        //spriteRenderers[(int)SpriteRenderers.HexTerrain].sprite = mSprite;
    }

    //public void Update()
    //{
        
    //}

    // TESTING CODE
    public void Test()
    {
        //StartCoroutine("OverlayCheck");
    }

    IEnumerator OverlayCheck()
    {
        //Debug.Log("Offset = " + (Position.OffsetPosition.x + Position.OffsetPosition.y).ToString());

        yield return new WaitForSeconds((Position.OffsetPosition.x + Position.OffsetPosition.y) / 2f);

        for (int i = 0;; i++)
        {
            if (i < 3)
            {
                SetMouseOverlay(HexMouseOverlay.None);
                SetMovementOverlay(HexMovementOverlay.None);
                //Debug.Log("VisibilityOverlay is now " + i.ToString());
                SetVisibilty((HexVisibility)i);
            }
            else if (i < 5)
            {
                SetVisibilty(HexVisibility.Visible);
                SetMouseOverlay(HexMouseOverlay.None);
                //Debug.Log("MovementOverlay is now " + ((i + 1) % 3).ToString());
                SetMovementOverlay((HexMovementOverlay)((i + 1) % 3));
            }
            else if (i < 6)
            {
                SetVisibilty(HexVisibility.Visible);
                SetMovementOverlay(HexMovementOverlay.None);
                //Debug.Log("MouseOverlay is now " + (i % 2).ToString());
                SetMouseOverlay((HexMouseOverlay)(i % 2));
            }
            else
            {
                SetVisibilty(HexVisibility.Invisible);
                i = 0;
            }

            yield return new WaitForSeconds(1f);
        }
    }
}
