﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// For HexTile Class

public enum HexVisibility
{
    Invisible,
    Explored,
    Visible
};

public enum HexMovementOverlay
{
    None,
    Move,
    Enemy
};

public enum HexMouseOverlay
{
    None,
    Highlighted
};

public enum HexType
{
    Sea,
    Plains,
    City,
    Forest
};