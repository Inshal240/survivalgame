//#define GRID_POSITIONS_TEXT
//#define TILES_2D
#define DEBUG_BAR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BoardManager : MonoBehaviour
{
    // Access to current instance of the board manager (with basic error handling)
    public static BoardManager Singleton
    {
        get
        {
            if (!_singleton)
                Debug.LogError("Singleton not set!");

            return _singleton;
        }
    }

    // Holds the prefab
    public GameObject tilePrefab;
    public TextureList[] HexTextures;

    // Getters for gridRows and gridCols
    public int GridRows { get { return gridRows; } }
    public int GridCols { get { return gridCols; } }

    // Allow external scripts to access grid information
    public HexTile[][] HexGrid
    {
        get { return hexGrid; }
    }
    

    // Pointer to class to enforce singleton behaviour
    private static BoardManager _singleton; 

    // Grid information will be stored here
    // NOTE: Tiles are stored column-wise and indexed by [col][row] 
    private HexTile[][] hexGrid;

    // Rows and columns of the grid
    private static int gridRows;
    private static int gridCols;
    

    // Use this for initialization
    void Start()
    {
        // Singleton class: Destroy this instance if there is another instance of 
        // BoardManager in the scene
        if (_singleton != null) Destroy(this);
        else _singleton = this;
    }

    void Update()
    {
        //if (UnityEditorInternal.ProfilerDriver.firstFrameIndex > 0)
        //{
        //    UnityEditorInternal.ProfilerDriver.ClearAllFrames();
        //}
    }

    // Initialize the hex grid
    public void InitializeHexGrid(int rows, int cols)
    {
        gridRows = rows;
        gridCols = cols;

        hexGrid = new HexTile[gridCols][];

        bool evenCol;       // boolean for even/odd column checks
        Vector3 center;     // Temp variable to hold center of a hex tile

        // Generate all the hexes in the grid
        for (int col = 0; col < gridCols; col++)
        {
            evenCol = (col % 2 == 0);

            // If the column is odd, assign it one less tile
            hexGrid[col] = new HexTile[(evenCol ? GridRows : GridRows - 1)];

            for (int row = 0; row < gridRows; row++)
            {
                // The following conidition skips the last hex on odd columns
                // (row + 1) < gridRows ensures that last hex is skipped in case of odd columns
                if (evenCol || (row + 1) < gridRows)
                {
                    // Instantiate the first prefab separately to initalize the size variables
                    if (row == 0 && col == 0)
                        center = Vector3.zero;
                    else
                        center = HexUtility.CalculateCenter(row, col, evenCol);

#if TILES_2D
                    hexGrid[col][row] = Instantiate(tilePrefab, center, Quaternion.Euler(0,0,90), transform).GetComponent<HexTile>();
#else
                    hexGrid[col][row] = Instantiate(tilePrefab, center, Quaternion.identity, transform).GetComponent<HexTile>();
#endif

                    // Rename the game object for ease
                    hexGrid[col][row].gameObject.name = "Tile[" + row + ", " + col + "]";

                    // Set hex tile properties
                    hexGrid[col][row].Position.OffsetPosition = new IntVect2(row, col);
                    hexGrid[col][row].HexCenter = center;

                    // TESTING CODE
                    hexGrid[col][row].SetVisibilty(HexVisibility.Explored);

#if GRID_POSITIONS_TEXT
                    GameObject textGO = new GameObject("TextforTile[" + row + ", " + col + "]");
                    textGO.transform.SetParent(hexGrid[col][row].gameObject.transform);
                    textGO.transform.position = new Vector3(center.x, center.y, -1);

                    TextMesh textMesh = textGO.AddComponent<TextMesh>() as TextMesh;

                    textMesh.text = hexGrid[col][row].OffsetPosition.ToString();

                    textMesh.characterSize = 0.3f;
                    textMesh.alignment = TextAlignment.Center;
                    textMesh.anchor = TextAnchor.MiddleCenter;
#endif
                }
            }
        }
    }

    // TESTING CODE
    public void MarkCenterHex()
    {
        hexGrid[GridCols / 2][GridRows / 2].SetMovementOverlay(HexMovementOverlay.Enemy);
        hexGrid[GridCols / 2][GridRows / 2].SetVisibilty(HexVisibility.Visible);
    }

    // TESTING CODE
    public void ResetGrid(bool complete = true)
    {
        for (int col = 0; col < hexGrid.Length; col++)
        {
            for (int row = 0; row < hexGrid[col].Length; row++)
            {
                if (hexGrid[col][row].Position.OffsetPosition != new IntVect2(8, 8))
                {
                    hexGrid[col][row].SetVisibilty(HexVisibility.Explored);

                    if (complete)
                    {
                        hexGrid[col][row].HexType = 0;
                        hexGrid[col][row].SetMouseOverlay(HexMouseOverlay.None);
                    }
                }
                else
                {
                    hexGrid[col][row].SetVisibilty(HexVisibility.Visible);

                    if (complete)
                    {
                        hexGrid[col][row].HexType = 0;
                        hexGrid[col][row].SetMouseOverlay(HexMouseOverlay.None);
                    }
                }
            }
        }
    }
    
    // TESTING CODE
    public void RandomizeGrid()
    {
        for (int col = 0; col < hexGrid.Length; col++)
        {
            for (int row = 0; row < hexGrid[col].Length; row++)
            {
                hexGrid[col][row].HexType = GetRandomHexType();
                // Highlight the obstacles on the map
                if (hexGrid[col][row].HexType > HexType.Plains)
                    hexGrid[col][row].SetMouseOverlay(HexMouseOverlay.Highlighted);
                else
                    hexGrid[col][row].SetMouseOverlay(HexMouseOverlay.None);
            }
        }
    }

    // TODO: Move elsewhere. BoardManager -> HexUtility
    private HexType GetRandomHexType()
    {
        return (HexType)Random.Range(0, (int)HexType.Forest);
    }

    //private void addHexTile()
    //{

    //}
}
