﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// For BoardManager class

[System.Serializable]
public class TextureList
{
    public Texture[] textures;
}

[System.Serializable]
public struct GridPosition
{
    [SerializeField]
    private IntVect3 position;

    public IntVect2 OffsetPosition
    {
        get { return HexUtility.CubeToOffset(position); }
        set { position = HexUtility.OffsetToCube(value); }
    }

    public IntVect3 CubeGridPosition
    {
        get { return position; }
        set { position = value; }
    }

    public IntVect2 AxialPosition
    {
        get { return HexUtility.CubeToAxial(position); }
        set { position = HexUtility.AxialToCube(value); }
    }
}

// For HexUtility Class

public struct Neighbours
{
    private static IntVect3[] offsetArr = 
    {
        new IntVect3(1, 0, -1),      // Top
        new IntVect3(0, 1, -1),      // TopRight
        new IntVect3(-1, 1, 0),      // BottomRight
        new IntVect3(-1, 0, 1),      // Bottom
        new IntVect3(0, -1, 1),      // BottomLeft
        new IntVect3(1, -1, 0)       // TopLeft
    };

    public static IntVect3[] OffsetArray { get { return offsetArr; } }

    // Individual neighbor offset
    public static IntVect3 Top           { get { return OffsetArray[0]; } }
    public static IntVect3 TopRight      { get { return OffsetArray[1]; } } 
    public static IntVect3 BottomRight   { get { return OffsetArray[2]; } }
    public static IntVect3 Bottom        { get { return OffsetArray[3]; } }
    public static IntVect3 BottomLeft    { get { return OffsetArray[4]; } }
    public static IntVect3 TopLeft       { get { return OffsetArray[5]; } }
};

public struct HexDirections
{
    private static Vector2[] directionArr =
    {
        new Vector2(0, 1),              // Up
        new Vector2(0.14425f, 1),
        new Vector2(0.2885f, 1),
        new Vector2(0.43275f, 1),
        new Vector2(0.577f, 1),         // TopRightCorner
        new Vector2(0.72125f, 1),
        new Vector2(0.7885f, 0.7885f),
        new Vector2(1, 0.72125f),
        new Vector2(1, 0.577f),         // TopRightSide
        new Vector2(1, 0.43275f),
        new Vector2(1, 0.2885f),
        new Vector2(1, 0.14425f),

        new Vector2(1, 0),              // Right
        new Vector2(1, -0.14425f),
        new Vector2(1, -0.2885f),
        new Vector2(1, -0.43275f),
        new Vector2(1, -0.577f),        // BottomRightSide
        new Vector2(1, -0.72125f),
        new Vector2(0.7885f, -0.7885f),
        new Vector2(0.72125f, -1),
        new Vector2(0.577f, -1),        // BottomRightCorner
        new Vector2(0.43275f, -1),
        new Vector2(0.2885f, -1),
        new Vector2(0.14425f, -1),

        new Vector2(-0, -1),            // Down
        new Vector2(-0.14425f, -1),
        new Vector2(-0.2885f, -1),
        new Vector2(-0.43275f, -1),
        new Vector2(-0.577f, -1),       // BottomLeftCorner
        new Vector2(-0.72125f, -1),
        new Vector2(-0.7885f, -0.7885f),
        new Vector2(-1, -0.72125f),
        new Vector2(-1, -0.577f),       // BottomLeftSide 
        new Vector2(-1, -0.43275f),
        new Vector2(-1, -0.2885f),
        new Vector2(-1, -0.14425f),

        new Vector2(-1, 0),              // Left
        new Vector2(-1, 0.14425f),
        new Vector2(-1, 0.2885f),
        new Vector2(-1, 0.43275f),
        new Vector2(-1, 0.577f),        // TopLeftSide
        new Vector2(-1, 0.72125f),
        new Vector2(-0.7885f, 0.7885f),
        new Vector2(-0.72125f, 1),
        new Vector2(-0.577f, 1),        // TopLeftCorner
        new Vector2(-0.43275f, 1),
        new Vector2(-0.2885f, 1),
        new Vector2(-0.14425f, 1),
    };

    public static Vector2[] Array { get { return directionArr; } }

    // Directions
    public static Vector2 Up { get { return Array[0]; } }
    public static Vector2 TopRightCorner { get { return Array[4]; } }
    public static Vector2 TopRightSide { get { return Array[8]; } }

    public static Vector2 Right { get { return Array[12]; } }
    public static Vector2 BottomRightSide { get { return Array[16]; } }
    public static Vector2 BottomRightCorner { get { return Array[20]; } }

    public static Vector2 Down { get { return Array[24]; } }
    public static Vector2 BottomLeftCorner { get { return Array[28]; } }
    public static Vector2 BottomLeftSide { get { return Array[32]; } }

    public static Vector2 Left { get { return Array[36]; } }
    public static Vector2 TopLeftSide { get { return Array[40]; } }
    public static Vector2 TopLeftCorner { get { return Array[44]; } }
};

// Use this for shifting ray casts up and down slightly
public struct ViewOffsets
{
    private const float offsetVal = 0.1f;

    private static Vector2[] offsetArr =
    {
        new Vector2(0, offsetVal),      // Up
        new Vector2(offsetVal, 0),      // Right
        new Vector2(0, -offsetVal),     // Down
        new Vector2(-offsetVal, 0)      // Left
    };

    public static Vector2[] Array { get { return offsetArr; } }

    public static Vector2 Up { get { return Array[0]; } }
    public static Vector2 Right { get { return Array[1]; } }
    public static Vector2 Down { get { return Array[2]; } }
    public static Vector2 Left { get { return Array[3]; } }
}