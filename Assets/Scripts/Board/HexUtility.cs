//#define TILES_2D

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

static public class HexUtility
{
    // Cube Conversions
    static public IntVect3 AxialToCube(IntVect2 mAxial)
    {
        return new IntVect3(
            mAxial.x,
            mAxial.y,
            -mAxial.x - mAxial.y
            );
    }

    static public IntVect3 OffsetToCube(IntVect2 mOffset)
    {
        IntVect3 gridPosition = new IntVect3();

        gridPosition.x = mOffset.x - (mOffset.y - ((byte)mOffset.y & 1)) / 2;
        gridPosition.y = mOffset.y;
        gridPosition.z = -gridPosition.x - gridPosition.y;

        return gridPosition;
    }

    // Axial Conversions
    static public IntVect2 CubeToAxial(IntVect3 mCube)
    {
        return new IntVect2(mCube.x, mCube.y);
    }

    static public IntVect2 OffsetToAxial(IntVect2 mOffset)
    {
        return new IntVect2(mOffset.x - (mOffset.y - ((byte)mOffset.y & 1)) / 2,
                            mOffset.y);
    }

    // Offset Conversions
    static public IntVect2 CubeToOffset(IntVect3 mCube)
    {
        return new IntVect2(mCube.x + (mCube.y - ((byte)mCube.y & 1)) / 2,
                            mCube.y);
    }

    static public IntVect2 AxialToOffset(IntVect2 mAxial)
    {
        return new IntVect2(mAxial.x + (mAxial.y - ((byte)mAxial.y & 1)) / 2,
                            mAxial.y);
    }

    // Calculate the center of the hex in world space
    public static Vector3 CalculateCenter(int mRow, int mCol, bool mEvenCol)
    {
        // row and col are switched below as row determines the height of the hex (along the y-axis)
        // whereas col determines how far away from the origin the hex is (along the x-axis)

        if (mEvenCol)   // Even Column
        {
            // The 0.75 offset is to overlap hexagons of different columns to fill in spaces
#if TILES_2D
            return new Vector3(
                mCol * 0.75f * HexTile.HexSize.y,
                mRow * HexTile.HexSize.x,
                0);
#else
            return new Vector3(
                    mCol * 0.75f * HexTile.HexSize.x,
                    0,
                    mRow * HexTile.HexSize.z);
#endif
        }
        else            // Odd Column
        {
#if TILES_2D
            return new Vector3(
                mCol * 0.75f * HexTile.HexSize.y,
                (mRow + 0.5f) * HexTile.HexSize.x,
                0);
#else
            return new Vector3(
                mCol * 0.75f * HexTile.HexSize.x,
                0,
                (mRow + 0.5f) * HexTile.HexSize.z);
#endif
        }
    }

    // Gets the neighbours of the input hex tile.
    // Returns an array of OffsetCoordinates to be used directly with the HexGrid
    static public IntVect2[] GetHexNeighbours(GridPosition mHexPosition)
    {
        List<IntVect2> neighbours = new List<IntVect2>();

        // Iterate over all the offsets and add them to the input position
        foreach (IntVect3 offset in Neighbours.OffsetArray)
        {
            IntVect2 n = CubeToOffset(mHexPosition.CubeGridPosition + offset);

            // Check whether the resultant coordinates are within the grid
            if (IsInGrid(n))
            {
                neighbours.Add(n);
            }
        }

        return neighbours.ToArray();
    }

    // TODO: TEST THIS
    // Gets distance between two hexes
    static public int GetHexDistance(GridPosition mFromHex, GridPosition mToHex)
    {
        IntVect3 diff = mFromHex.CubeGridPosition - mToHex.CubeGridPosition;
        return (int)Mathf.Max(Mathf.Abs(diff.x), Mathf.Abs(diff.y), Mathf.Abs(diff.z));
    }

    // Returns an array of the offest coordinates of all hextiles in the given range
    static public IntVect2[] GetAllHexInRange(GridPosition mHexPosition, int mRange)
    {
        List<IntVect2> hexInRangeList = new List<IntVect2>();

        for (int dx = -mRange; dx <= mRange; dx++)
        {
            for (int dy = Mathf.Max(-mRange, -dx - mRange); dy <= Mathf.Min(mRange, -dx + mRange); dy++)
            {
                int dz = -dx - dy;

                IntVect2 hexInRange = 
                    CubeToOffset(mHexPosition.CubeGridPosition + new IntVect3(dx, dy, dz));

                if (IsInGrid(hexInRange)) hexInRangeList.Add(hexInRange);
            }
        }

        return hexInRangeList.ToArray();
    }

    static public IntVect2[] GetAllHexInView(GridPosition mHexPosition, int mRange)
    {
        HashSet<IntVect2> hexInViewSet = new HashSet<IntVect2>
        {
            // The current tile will always be in sight
            mHexPosition.OffsetPosition
        };

        // If the input range is zero then exit the function
        if (mRange < 1) return hexInViewSet.ToArray();

        // Distance from current hex to furthest possible hex (-0.25f to prevent overshooting)
        float rayLength = HexTile.HexSize.y * (mRange - 0.25f);

        // Fetch the center of the input hex tile
        var board = BoardManager.Singleton;

        Vector2 hexCenter = board.HexGrid[mHexPosition.OffsetPosition.y]
                                               [mHexPosition.OffsetPosition.x].HexCenter;

        // Holds the colliders that the raycast hit
        RaycastHit2D[] hits;

        // Iterate over all directions in the hexDirection struct
        foreach (Vector2 direction in HexDirections.Array)
        {
            Debug.DrawRay(hexCenter, direction * mRange, Color.red, 4);

            // Skip the first element which is the current tile
            hits = Physics2D.RaycastAll(hexCenter, direction, rayLength,
                LayerMask.GetMask("Terrain")).Skip(1).ToArray();

            foreach (RaycastHit2D hit in hits)
            {
                HexTile t = hit.collider.GetComponent<HexTile>();
                hexInViewSet.Add(t.Position.OffsetPosition);

                if (t.HexType > HexType.Plains)
                    break;
            }
        }

        return hexInViewSet.ToArray();
    }

    static public IntVect2[] GetPossibleMoves(GridPosition mHexPosition, int mRange)
    {
        return null;
    }

    // Helper functions

    // Checks whether the input offset coordinates are in within the grid boundaries
    static private bool IsInGrid(IntVect2 mOffsetCoords)
    {
        var board = BoardManager.Singleton;

        // For even columns
        if (mOffsetCoords.y % 2 == 0)
            return mOffsetCoords.x > -1 && mOffsetCoords.y > -1
                && mOffsetCoords.x < board.GridRows 
                && mOffsetCoords.y < board.GridCols;
        // For odd columns
        else
            return mOffsetCoords.x > -1 && mOffsetCoords.y > -1
                && mOffsetCoords.x < (board.GridRows - 1)
                && mOffsetCoords.y < board.GridCols;
    }
}
