﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Implement and find a better name. Item's info in container that the latter can use.
//public struct ItemInContainerInfo
//{
//    public int ItemIndex;                   // Index of item in the inventory list
//    public IntVect2 OriginSlot;             // Source box of the item
//    public ItemOrientation Orientation;     // Orientation of the item image
//    public int StackSize;

//    public enum ItemOrientation
//    {
//        ZeroDegrees,
//        NinetyDegrees,
//        OneEightyDegrees,
//        TwoSeventyDegrees
//    }
//}

// Inventory slots
public struct InventorySlot
{
    public bool IsOccupied;
    public Rect PositionRect;
    public bool IsRootSlot;
    public int ItemIndex;
    public int StackSize;
    
    public static float SlotSize;
}

// Item display information
public struct ItemDisplayInfo
{
    private Texture2D itemTexture;
    private ItemOrientation orientation;

    public Rect ItemRect;
    public IntVect2 RootSlotPosition;
    public IntVect2 ItemGridSize;

    public Texture2D ItemTexture { get { return itemTexture; } }
    public ItemOrientation Orientation { get { return orientation; } }

    // Set texture of the struct
    public void SetTexture(Texture2D mTexture, ItemOrientation mOrientation = ItemOrientation.ZeroDegrees)
    {
        itemTexture = mTexture;

        if (mOrientation != ItemOrientation.ZeroDegrees)
        {
            SetOrientation(mOrientation);
        }
        else orientation = mOrientation;
    }

    // Sets orietntation of the image and grid size
    public void SetOrientation(ItemOrientation mOrientation)
    {
        // Call rotateTexture until orientation == mOrientation
        while (orientation != mOrientation)
        {
            RotateItem();
        }
    }
    
    // Rotates item clockwise
    public void RotateItem()
    {
        if (itemTexture == null)
            return;
        
        // Create a new texture with its dimensions inverted
        Texture2D target = new Texture2D(itemTexture.height, itemTexture.width, itemTexture.format, false);

        Color32[] pixels = itemTexture.GetPixels32(0);

        Color32[] ret = new Color32[itemTexture.height * itemTexture.width];

        for (int y = 0; y < itemTexture.height; y++)
        {
            for (int x = 0; x < itemTexture.width; x++)
            {
                // Reorder pixels in the new texture
                ret[(itemTexture.width - 1 - x) * itemTexture.height + y] = pixels[x + y * itemTexture.width];
            }
        }

        // Set and apply changes
        target.SetPixels32(ret);
        target.Apply();

        // Update struct variables
        orientation = (ItemOrientation)(((int)orientation + 1) % 4);
        itemTexture = target;
        ItemGridSize.Set(ItemGridSize.y, ItemGridSize.x);
        ItemRect = new Rect(0, 0, ItemRect.height, ItemRect.width);
    }
}