﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

// TODO:
public class GameItem
{
    // General inventory information
    protected string name;
    protected int uiid;
    protected string description;
    protected Texture2D inventoryTexture;
    protected Texture2D equippedTexture;
    protected Texture2D foldedTexture;
    
    // Inventory management information
    protected int maxStackSize;
    protected IntVect2 inventorySize;
    protected bool isFoldable;
    protected IntVect2 foldedSize;

    // Spawn Information
    protected string[] spawnLocations;
    protected float[] spawnChances;

    // Bools for recognizing uses of game item
    protected bool isEdible;
    protected bool isWeapon;
    protected bool isWearable;
    protected bool isContainer;
    protected bool isAmmunition;
    protected bool isLiquid;
    protected bool isDestructible;

    // Structs to retrieve relevant information about the game item
    protected CraftingInfo craftingInfo;
    protected EdibleInfo edibleInfo;
    protected WeaponInfo weaponInfo;
    protected AmmoInfo ammoInfo;
    protected WearableInfo wearableInfo;
    protected ContainerInfo containerInfo;
    protected MedicinalInfo medicinalInfo;
    // protected StatsInfo statsInfo;

    // Public accessors
    public string Name { get { return name; } }
    public int UIID { get { return uiid; } }
    public string Description { get { return description; } }
    public Texture2D InventoryTexture { get { return inventoryTexture; } }
    public Texture2D EquippedTexture { get { return equippedTexture; } }
    public Texture2D FoldedTexture { get { return foldedTexture; } }

    public int MaxStackSize { get { return maxStackSize; } }
    public IntVect2 InventorySize { get { return inventorySize; } }
    public bool IsFoldable { get { return isFoldable; } }
    public IntVect2 FoldedSize { get { return foldedSize; } }

    public string[] SpawnLocation { get { return spawnLocations; } }
    public float[] SpawnChances { get { return spawnChances; } }

    public bool IsWeapon { get { return isWeapon; } }
    public bool IsWearable { get { return isWearable; } }
    public bool IsContainer { get { return isContainer; } }
    public bool IsAmmunition { get { return isAmmunition; } }
    public bool IsLiquid { get { return isLiquid; } }
    public bool IsEdible { get { return isEdible; } }
    public bool IsDestructible { get { return isDestructible; } }

    // Interface implementations
    public EdibleInfo GetEdibleInfo()
    {
        throw new NotImplementedException();
    }
    
    public WeaponInfo GetWeaponInfo()
    {
        throw new NotImplementedException();
    }

    public AmmoInfo GetAmmoInfo()
    {
        throw new NotImplementedException();
    }

    public WearableInfo GetWearableInfo()
    {
        throw new NotImplementedException();
    }

    public ContainerInfo GetContainerInfo()
    {
        throw new NotImplementedException();
    }

    public MedicinalInfo GetMedicinalInfo()
    {
        throw new NotImplementedException();
    }

    // Initialize an item from a file
    public void ReadItemFromFile(string mFilename)
    {
        
    }

    // TESTING CODE
    public static GameItem CreateTestItem()
    {
        GameItem item = new GameItem();

        // General inventory information
        item.name = "Test Item";
        item.uiid = 123;
        item.description = "Test item is used to test the inventory system.";
        item.inventoryTexture = Resources.Load<Texture2D>("Game Items/Tex2D_Katana");

        if (item.InventoryTexture == null)
            Debug.Log("Failed to load image");

        // Inventory management information
        item.maxStackSize = 1;
        item.inventorySize = new IntVect2(2, 8);
        item.isFoldable = false;

        return item;
    }

    public static GameItem CreateTestItem2()
    {
        GameItem item = new GameItem();
        
        // General inventory information
        item.name = "Test Item";
        item.uiid = 123;
        item.description = "Test item is used to test the inventory system.";
        item.inventoryTexture = Resources.Load<Texture2D>("Game Items/Grid4x4");

        if (item.InventoryTexture == null)
            Debug.Log("Failed to load image");

        // Inventory management information
        item.maxStackSize = 1;
        item.inventorySize = new IntVect2(4, 4);
        item.isFoldable = false;

        return item;
    }
}