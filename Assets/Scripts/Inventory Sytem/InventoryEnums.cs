﻿ using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemOrientation
{
    ZeroDegrees,
    NinetyDegrees,
    OneEightyDegrees,
    TwoSeventyDegrees
}