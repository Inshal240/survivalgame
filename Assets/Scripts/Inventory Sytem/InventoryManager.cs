//#define DEBUG_DRAGGING
//#define DEBUG_DRAG_DROP
//#define DEBUG_SHOWROOT
//#define DEBUG_SHOWINDEX
#define DEBUG_CONTAINERRECTS

using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;


[ExecuteInEditMode]
public class InventoryManager : MonoBehaviour
{
    // TODO: Set this using a PlayerInput class
    private bool showInventory = false;

    // Item dragging variables
    private bool draggingItem = false;
    private GameItem draggedItem;
    private ItemContainer draggedItemContainer;
    private ItemDisplayInfo draggedItemDisplayInfo;

    // TESTING CODE
    // Container variables
    private List<ItemContainer> playerContainers = new List<ItemContainer>();

    [SerializeField]
    private ItemContainer groundContainer;      // Holds reference to ground container; assigned by current tile

    // Inventory Dimensions
    public GUISkin InventorySkin;

    // UI Elements
    public GameObject inventoryPanel;
    public ScrollRect leftScrollRect;
    public ScrollRect rightScrollRect;

    // TESTING CODE
    public int rows = 50;
    public int cols = 20;

    //int GROUND_SIZE_ROWS = 50;
    //int GROUND_SIZE_COLS = 20;
    //int PANEL_WIDTH = 200;
    //int X_PADDING = 20;
    //int Y_PADDING = 60;
    //int SCROLLBAR_SIZE = 16;
    //float INV_TO_GRND_RATIO = 0.4f;

    // Use this for initialization
    void Start()
    {
        // The first element of containers list is the ground
        //playerContainers.Add(new ItemContainer());
        //groundContainer = new ItemContainer(rows, cols);
        //playerContainers.Add(new ItemContainer(rows, cols));
        //CalculateInventoryDimensions();

        groundContainer = GetComponentsInChildren<ItemContainer>()[0];
        playerContainers.Add(GetComponentsInChildren<ItemContainer>()[1]);
        CalculateSlotDimensions();

        // TESTING CODE
        if (groundContainer.AddItem(GameItem.CreateTestItem(), ItemOrientation.ZeroDegrees) == -1)
            Debug.Log("Insertion failed");

        inventoryPanel.SetActive(showInventory);
    }

    public void ToggleInventory()
    {
        showInventory = !showInventory;
        inventoryPanel.SetActive(showInventory);

        if (showInventory) CalculateSlotDimensions();
    }

    // Update is called once per frame
    void Update()
    {
        //if(Input.GetButtonUp("Inventory"))
        //{
        //    showInventory = !showInventory;
        //    print("Show Inventory set to " + showInventory.ToString());

        //    // TODO: Call this function only when screen dimensions are changed
        //    //CalculateInventoryDimensions();
        //    CalculateSlotDimensions();
        //}

        if (showInventory)
        {
            if (Input.GetKeyUp(KeyCode.Alpha1))
            {
                groundContainer.AddItem(GameItem.CreateTestItem());
            }

            if(Input.GetKeyUp(KeyCode.Alpha2))
            {
                groundContainer.AddItem(GameItem.CreateTestItem2());
            }
        }
    }

    void OnGUI()
    {
        if (showInventory)
        {
            GUI.skin = InventorySkin;
            Event e = Event.current;
            
            // Show ground container
            DrawContainer(groundContainer, e);

            // Show player containers
            DrawContainer(playerContainers[0], e);
            
            // Dragged item handler
            if (draggingItem)
            {
                if (e.button == 1 && e.type == EventType.MouseUp)
                {
                    draggedItemDisplayInfo.RotateItem();
                }

                // In case no container could handle the drop item event
                // i.e. item was dropped outside all containers
                if (e.button == 0 && e.type == EventType.MouseUp)
                {
                    Debug.Log("Item dropped outside. Restoring.");
                    
                    RestoreDraggedItem();
                    ResetDragDropVariables();
                }

                DrawDraggedItem(e);
            }
        }
    }

    private void CalculateSlotDimensions()
    {
        // Get slot size
        InventorySlot.SlotSize = leftScrollRect.viewport.rect.x / cols;

        if (groundContainer != null)
        {
            // Ground container calculations
            //groundContainer.ContainerRect = leftScrollRect.content;
            //groundContainer.ViewPortRect = leftScrollRect.viewport;

            groundContainer.InitializeSlotRects();

#if DEBUG
            // TESTING CODE
            //Debug.Log("Slot size calculated to be = " + InventorySlot.SlotSize.ToString());
#endif
        }


        // TESTING CODE, player containers will be generated according to items equipped
        //playerContainers[0].ContainerRect = rightScrollRect.content;
        //playerContainers[0].ViewPortRect = rightScrollRect.viewport;

        playerContainers[0].InitializeSlotRects();
    }

    // TODO: Calculate all dimension changes here
    /// <summary>
    /// Fix dimensions in case of screen resolution changes
    /// </summary>
    //    public void CalculateInventoryDimensions()
    //    {
    //        // TODO: Calculate/Get PANEL_WIDTH from Status/HUD window
    //        // TODO: Calculate padding sizes (for scalibility)

    //        // TODO: Add left status panel in calculations
    //        inventoryRect = new Rect(PANEL_WIDTH, 0, Screen.width, Screen.height);

    //        if (groundContainer != null)
    //        {
    //            // Inventory screen size halved, subtracted space for padding, divided the rest among 20 slots
    //            InventorySlot.SlotSize = (int)(((Screen.width - PANEL_WIDTH) *
    //                INV_TO_GRND_RATIO - 2 * X_PADDING) / groundContainer.GridSize.y);

    //            // Ground container calculations
    //            groundContainer.ContainerRect = new Rect(0, 0,
    //                   InventorySlot.SlotSize * groundContainer.GridSize.y,
    //                   InventorySlot.SlotSize * groundContainer.GridSize.x);

    //            groundContainer.ViewPortRect = new Rect(
    //                    inventoryRect.x + X_PADDING,
    //                    inventoryRect.y + Y_PADDING,
    //                    groundContainer.ContainerRect.width + SCROLLBAR_SIZE,
    //                    inventoryRect.height - X_PADDING - Y_PADDING);

    //            groundContainer.CalculateSlotRects();

    //#if DEBUG
    //            // TESTING CODE
    //            //Debug.Log("Slot size calculated to be = " + InventorySlot.SlotSize.ToString());
    //#endif
    //        }


    //        // TESTING CODE, player containers will be generated according to items equipped
    //        playerContainers[0].ContainerRect = groundContainer.ContainerRect;
    //        playerContainers[0].ViewPortRect = groundContainer.ViewPortRect;

    //        playerContainers[0].ViewPortRect.x += InventorySlot.SlotSize
    //            * groundContainer.GridSize.y + SCROLLBAR_SIZE + X_PADDING * 2;

    //        playerContainers[0].CalculateSlotRects();
    //    }

    public void DrawContainer(ItemContainer mContainer, Event mEvent)
    {
        // Scroll view
        //mContainer.ScrollPosition = GUI.BeginScrollView(mContainer.ViewPortRect,
        //    mContainer.ScrollPosition, mContainer.ContainerRect, false, false);
        
        // Use this rect to highlight item drop area
        Rect dropArea = new Rect();

        for (int row = 0; row < mContainer.GridSize.row; row++)
        {
            for (int col = 0; col < mContainer.GridSize.col; col++)
            {
                // Dragged item's top left corner is over the slot
                if (draggingItem && 
                    mContainer.Slots[row, col].PositionRect.Contains(
                    new Vector2(mEvent.mousePosition.x - draggedItemDisplayInfo.ItemRect.width * 0.5f, 
                    mEvent.mousePosition.y - draggedItemDisplayInfo.ItemRect.height * 0.5f)))
                {
                    // If slot is not occupied and there is space for dragged item...
                    if (!mContainer.Slots[row, col].IsOccupied && mContainer.HasRoomForItem(row, col,
                        draggedItemDisplayInfo.ItemGridSize.col, draggedItemDisplayInfo.ItemGridSize.row))
                    {
                        // ... set up the highlight area space
                        dropArea.x = row;
                        dropArea.y = col;
                        dropArea.width = draggedItemDisplayInfo.ItemGridSize.row;
                        dropArea.height = draggedItemDisplayInfo.ItemGridSize.col;
                    }

                    // Item is being dropped in into the slot
                    if (mEvent.button == 0 && mEvent.type == EventType.MouseUp)
                    {
#if DEBUG_DRAG_DROP
                        Debug.Log("Dropping item at " + row + ", " + col);
#endif
                        if (mContainer.AddItemAtPosition(draggedItem, row, col, draggedItemDisplayInfo.Orientation) == -1)
                        {
#if DEBUG_DRAG_DROP
                            Debug.Log("Drop failed. Restoring to original container");
#endif
                            // Item insertion into new container failed, so put it back where it was
                            RestoreDraggedItem();
                        }

                        ResetDragDropVariables();
                    }
                }

                if (mContainer.Slots[row, col].PositionRect.Contains(mEvent.mousePosition))
                {
#if DEBUG_DRAGGING
                    Debug.Log("Mouse in slot " + new Vector2(row, col));
#endif
                    // Dragging item
                    if (!draggingItem && mContainer.Slots[row, col].IsOccupied
                        && mEvent.button == 0 && mEvent.type == EventType.MouseDrag)
                    {
                        int draggedItemIdx = mContainer.Slots[row, col].ItemIndex;

#if DEBUG_DRAGGING
                        Debug.Log("Started dragging from " + row + ", " + col + " with item index " + draggedItemIdx + ".");
#endif

                        draggingItem = true;
                        draggedItemContainer = mContainer;
                        draggedItem = mContainer.RemoveItem(draggedItemIdx, out draggedItemDisplayInfo);

                        if(draggedItem == null)
                        {
                            throw new Exception("No item found at index " + draggedItemIdx + "!");
                        }
                    }
                }

                GUIStyle boxStyle;

                // Change skin if slot is occupied
#if DEBUG_SHOWROOT
                if (mContainer.Slots[row, col].IsRootSlot)
                {
                    boxStyle = InventorySkin.GetStyle("ContainerSlotRoot");
                }
                else
#endif
                if (mContainer.Slots[row, col].IsOccupied)
                {
                    boxStyle = InventorySkin.GetStyle("ContainerSlotDark");
                }
                // If slot is within the drop area and not occupied then highlight it
                else if (dropArea.Contains(new Vector2(row, col)))
                {
                    boxStyle = InventorySkin.GetStyle("ContainerSlotHighlight");
                }
                // Normal slot
                else
                {
                    boxStyle = InventorySkin.GetStyle("ContainerSlot");
                }
                
                GUI.Box(mContainer.Slots[row, col].PositionRect,
#if DEBUG_SHOWINDEX
                    mContainer.Slots[row, col].ItemIndex +
#endif
                    "",
                    boxStyle);
            }
        }

        // NOTE: This needs to be separate as the drawing items along with their slots
        // means that the item texture of the previous slot will be drawn before the
        // the next slot itself, causing items to be drawn behind all following slots
        DrawItemsInContainer(mContainer, mEvent);

        //GUI.EndScrollView();
    }
    
    // Draws items in inventory
    public void DrawItemsInContainer(ItemContainer mContainer, Event mEvent)
    {
        // Inverse loop as the contents of mContainer.ItemDisplayInfos could be
        // modified due to items being removed on drag
        for (int idx = mContainer.ItemDisplayInfos.Count - 1; idx > -1; idx--)
        {
            if(mContainer.ItemDisplayInfos[idx].ItemTexture != null)
            {
                GUI.DrawTexture(mContainer.ItemDisplayInfos[idx].ItemRect, 
                    mContainer.ItemDisplayInfos[idx].ItemTexture);
            }
        }
    }

    public void DrawDraggedItem(Event mEvent)
    {
        if (draggedItem != null && draggedItemDisplayInfo.ItemTexture != null)
        {
            draggedItemDisplayInfo.ItemRect.x =
                mEvent.mousePosition.x - draggedItemDisplayInfo.ItemRect.width * 0.5f;
            draggedItemDisplayInfo.ItemRect.y =
                mEvent.mousePosition.y - draggedItemDisplayInfo.ItemRect.height * 0.5f;

            GUI.DrawTexture(draggedItemDisplayInfo.ItemRect,
                draggedItemDisplayInfo.ItemTexture);

            // TESTING CODE
            //GUI.Box(draggedItemDisplayInfo.ItemRect,
            //    draggedItemDisplayInfo.ItemTexture);
        }
    }

    // TODO: Add container when player equips a bag
    public void AddContainer(GameItem mItem)
    {
        throw new NotImplementedException();
    }

    public GameItem RemoveContainer(GameItem item)
    {
        throw new NotImplementedException();
    }

    public void SetGroudContainer(ItemContainer mGroundContainer)
    {
        throw new NotImplementedException();
    }

    // HELPER FUNCTIONS

    private void ResetDragDropVariables()
    {
        // Reset all variables
        draggingItem = false;
        draggedItemContainer = null;
        draggedItem = null;
        draggedItemDisplayInfo = new ItemDisplayInfo();
    }

    private void RestoreDraggedItem()
    {
        Debug.Log("Item restored to " + draggedItemDisplayInfo.RootSlotPosition.row + ", " +
            draggedItemDisplayInfo.RootSlotPosition.col);

        draggedItemContainer.AddItemAtPosition(
            draggedItem,
            draggedItemDisplayInfo.RootSlotPosition.row,
            draggedItemDisplayInfo.RootSlotPosition.col,
            draggedItemDisplayInfo.Orientation);
    }
}
