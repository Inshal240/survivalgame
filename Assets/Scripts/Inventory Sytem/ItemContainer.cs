﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(ScrollRect))]
public class ItemContainer : MonoBehaviour
{
    public Sprite normalSlotImage;
    public Sprite occupiedSlotImage;
    public Sprite highlightedSlotImage;

    public IntVect2 GridSize = new IntVect2(50, 15);       // Capacity of the item, row x col
    public List<GameItem> Items = new List<GameItem>();
    public List<ItemDisplayInfo> ItemDisplayInfos = new List<ItemDisplayInfo>();

    [SerializeField]
    public InventorySlot[,] Slots;

    //public static int SlotSize;
    //public enum EquipmentSlot; // for backpack or pant pockets, etc

    //public Vector2 ScrollPosition;
    //public RectTransform ViewPortRect;
    //public RectTransform ContainerRect;
    private RectTransform contentRect;

    //public void Start()
    //{
    //    contentRect = GetComponent<ScrollRect>().content;
    //}

    public void Start()
    {
        //GridSize = new IntVect2(mGridRows, mGridCols);    // 50 rows, 20 cols
        Slots = new InventorySlot[GridSize.x, GridSize.y];
        contentRect = GetComponent<ScrollRect>().content;
    }

    // Calculates the slot rects. Call this whenever InventorySlot.SlotSize is changed
    public void InitializeSlotRects()
    {
        GridLayoutGroup grid = GetComponentInChildren<GridLayoutGroup>();
        float slotSize = grid.cellSize.x;

        for (int row = 0; row < GridSize.x; row++)
        {
            for (int col = 0; col < GridSize.y; col++)
            {
                Slots[row, col].PositionRect = new Rect(
                    contentRect.rect.x + col * slotSize,
                    contentRect.rect.y + row * slotSize,
                    slotSize, slotSize);
            }
        }
    }

    /// <summary>
    /// Adds item to the container at a specific position while also performing checks to 
    /// ensure safe insertion.
    /// </summary>
    /// <param name="mItem"></param>
    /// <param name="mSlotRow"></param>
    /// <param name="mSlotCol"></param>
    /// <param name="mOrientation"></param>
    /// <returns></returns>
    public int AddItemAtPosition(GameItem mItem, int mSlotRow, int mSlotCol,
        ItemOrientation mOrientation = ItemOrientation.ZeroDegrees)
    {
        // Check for game item and grid position
        if (mItem == null)
            return -1;

        // Set item size in inventory according to orientation
        int itemHeight = mItem.InventorySize.row,
            itemWidth = mItem.InventorySize.col;

        if (mOrientation == ItemOrientation.NinetyDegrees ||
            mOrientation == ItemOrientation.TwoSeventyDegrees)
        {
            itemHeight = mItem.InventorySize.col;
            itemWidth = mItem.InventorySize.row;
        }

        // Check for space in inventory
        if (!HasRoomForItem(mSlotRow, mSlotCol, itemWidth, itemHeight))
            return -1;

        return addItemAtPosition(mItem, mSlotRow, mSlotCol, itemWidth, itemHeight, mOrientation);
    }

    /// <summary>
    /// Adds item to first possible location in the container.
    /// </summary>
    /// <param name="mItem"></param>
    /// <returns></returns>
    public int AddItem(GameItem mItem, ItemOrientation mOrientation = ItemOrientation.ZeroDegrees)
    {
        // Check for game item and grid position
        if (mItem == null)
            return -1;

        // Set item size in inventory according to orientation
        int itemHeight = mItem.InventorySize.row,
            itemWidth = mItem.InventorySize.col;

        if (mOrientation == ItemOrientation.NinetyDegrees ||
            mOrientation == ItemOrientation.TwoSeventyDegrees)
        {
            itemHeight = mItem.InventorySize.col;
            itemWidth = mItem.InventorySize.row;
        }

        // Possible position of for new item
        int slotRow = 0, slotCol = 0;
        bool slotFound = false;

        // Look for space in inventory
        for (slotRow = 0; (slotRow + itemHeight - 1) < GridSize.row; slotRow++)
        {
            for (slotCol = 0; (slotCol + itemWidth - 1) < GridSize.col; slotCol++)
            {
                if (HasRoomForItem(slotRow, slotCol, itemWidth, itemHeight))
                {
                    slotFound = true;
                    break;
                }
            }

            if (slotFound) break;
        }

        if (!slotFound)
            return -1;
        else
            return addItemAtPosition(mItem, slotRow, slotCol, itemWidth, itemHeight, mOrientation);
    }

    /// <summary>
    /// Removes item from container and returns ItemDisplayInfo struct as well.
    /// </summary>
    /// <param name="mItemIdx"></param>
    /// <param name="mItemDisplayInfo"></param>
    /// <returns></returns>
    public GameItem RemoveItem(int mItemIdx, out ItemDisplayInfo mItemDisplayInfo)
    {
        // Check for game item and grid position
        if (mItemIdx < 0 || mItemIdx > Items.Count)
        {
            mItemDisplayInfo = new ItemDisplayInfo();
            return null;
        }

        // Set return values
        GameItem returnItem = Items[mItemIdx];
        mItemDisplayInfo = ItemDisplayInfos[mItemIdx];

        // Free the occupied space in the container
        int itemHeight = returnItem.InventorySize.x,
            itemWidth = returnItem.InventorySize.y;

        if (mItemDisplayInfo.Orientation == ItemOrientation.NinetyDegrees ||
            mItemDisplayInfo.Orientation == ItemOrientation.TwoSeventyDegrees)
        {
            itemHeight = returnItem.InventorySize.y;
            itemWidth = returnItem.InventorySize.x;
        }

        freeSlots(mItemDisplayInfo.RootSlotPosition.row,
            mItemDisplayInfo.RootSlotPosition.col, itemWidth, itemHeight);

        ItemDisplayInfos.RemoveAt(mItemIdx);
        Items.RemoveAt(mItemIdx);

        // TESTING CODE
        // Update indices of affected items after the removal
        updateIndices(mItemIdx);

        return returnItem;
    }

    /// <summary>
    /// Removes item from container.
    /// </summary>
    /// <param name="mItemIdx"></param>
    /// <returns></returns>
    public GameItem RemoveItem(int mItemIdx)
    {
        // Check for game item and grid position
        if (mItemIdx < 0 || mItemIdx > Items.Count)
        {
            return null;
        }

        // Set return values
        GameItem returnItem = Items[mItemIdx];
        ItemDisplayInfo itemDisplayInfo = ItemDisplayInfos[mItemIdx];

        // Free the occupied space in the container
        int itemHeight = returnItem.InventorySize.row,
            itemWidth = returnItem.InventorySize.col;

        if (itemDisplayInfo.Orientation == ItemOrientation.NinetyDegrees ||
            itemDisplayInfo.Orientation == ItemOrientation.TwoSeventyDegrees)
        {
            itemHeight = returnItem.InventorySize.col;
            itemWidth = returnItem.InventorySize.row;
        }

        freeSlots(itemDisplayInfo.RootSlotPosition.row,
            itemDisplayInfo.RootSlotPosition.col, itemWidth, itemHeight);

        ItemDisplayInfos.RemoveAt(mItemIdx);
        Items.RemoveAt(mItemIdx);

        // TESTING CODE
        // Update indices of affected items after the removal
        updateIndices(mItemIdx);

        return returnItem;
    }

    // HELPER FUNCTIONS

    /// <summary>
    /// Checks for available space in inventory.
    /// </summary>
    /// <param name="mStartRow"></param>
    /// <param name="mStartCol"></param>
    /// <param name="mItemWidth"></param>
    /// <param name="mItemHeight"></param>
    /// <returns></returns>
    public bool HasRoomForItem(int mStartRow, int mStartCol, int mItemWidth, int mItemHeight)
    {
        // Check if out of bounds
        if (mStartRow < 0 || mStartCol < 0 || mItemHeight < 0 || mItemWidth < 0 ||
            (mStartRow + mItemHeight) > GridSize.row ||
            (mStartCol + mItemWidth) > GridSize.col)
            return false;

        // Check if there is enough space at the position for the item
        for (int row = 0; row < mItemHeight; row++)
        {
            for (int col = 0; col < mItemWidth; col++)
            {
                if (Slots[mStartRow + row, mStartCol + col].IsOccupied)
                    return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Adds item to a specific position in the container without any checks.
    /// </summary>
    /// <param name="mItem"></param>
    /// <param name="mSlotRow"></param>
    /// <param name="mSlotCol"></param>
    /// <param name="mItemWidth"></param>
    /// <param name="mItemHeight"></param>
    /// <param name="mOrientation"></param>
    /// <returns></returns>
    private int addItemAtPosition(GameItem mItem, int mSlotRow, int mSlotCol, int mItemWidth, int mItemHeight,
        ItemOrientation mOrientation = ItemOrientation.ZeroDegrees)
    {
        // Add item to container list
        Items.Add(mItem);

        // Set the remaining properties of the slot
        int itemIdx = Items.Count - 1;

        // Evaluate item display information and add it to container's list
        ItemDisplayInfo itemDisplay = new ItemDisplayInfo();

        // TODO: Add checks for folding items
        itemDisplay.ItemGridSize = mItem.InventorySize;
        itemDisplay.SetTexture(mItem.InventoryTexture, mOrientation);

        itemDisplay.ItemRect = new Rect(
            Slots[mSlotRow, mSlotCol].PositionRect.x,
            Slots[mSlotRow, mSlotCol].PositionRect.y,
            InventorySlot.SlotSize * mItemWidth,
            InventorySlot.SlotSize * mItemHeight);

        itemDisplay.RootSlotPosition = new IntVect2(mSlotRow, mSlotCol);

        ItemDisplayInfos.Add(itemDisplay);

        // Update all affected slots
        occupySlots(mSlotRow, mSlotCol, mItemWidth, mItemHeight, itemIdx);

#if DEBUG
        //Debug.Log("Test item added at " + mSlotRow + ", " + mSlotCol);
        //Debug.Log("Item rect = " + ItemDisplayInfos[itemIdx].ItemRect.ToString());
#endif
        return itemIdx;
    }

    /// <summary>
    /// Occupies slots.
    /// </summary>
    /// <param name="mStartRow"></param>
    /// <param name="mStartCol"></param>
    /// <param name="mItemWidth"></param>
    /// <param name="mItemHeight"></param>
    /// <param name="mItemIdx"></param>
    private void occupySlots(int mStartRow, int mStartCol, int mItemWidth, int mItemHeight, int mItemIdx)
    {
        // Set root slot property
        Slots[mStartRow, mStartCol].IsRootSlot = true;

        // Occcupy all slots
        for (int row = 0; row < mItemHeight; row++)
        {
            for (int col = 0; col < mItemWidth; col++)
            {
                Slots[mStartRow + row, mStartCol + col].IsOccupied = true;
                Slots[mStartRow + row, mStartCol + col].ItemIndex = mItemIdx;
            }
        }
    }

    private void freeSlots(int mStartRow, int mStartCol, int mItemWidth, int mItemHeight)
    {
        // Set root slot property
        Slots[mStartRow, mStartCol].IsRootSlot = false;

        // Free all slots
        for (int row = 0; row < mItemHeight; row++)
        {
            for (int col = 0; col < mItemWidth; col++)
            {
                Slots[mStartRow + row, mStartCol + col].IsOccupied = false;
                Slots[mStartRow + row, mStartCol + col].ItemIndex = 0;
            }
        }
    }

    void updateIndices(int mIndex)
    {
        // TODO: Make index updates more efficient
        for (int idx = mIndex; idx < ItemDisplayInfos.Count; idx++)
        {
            IntVect2 slotPos = ItemDisplayInfos[idx].RootSlotPosition;

            // Update all slots that hold the current item
            for (int rowOffset = 0; rowOffset < ItemDisplayInfos[idx].ItemGridSize.row; rowOffset++)
            {
                for (int colOffset = 0; colOffset < ItemDisplayInfos[idx].ItemGridSize.col; colOffset++)
                {
                    Slots[slotPos.row + rowOffset, slotPos.col + colOffset].ItemIndex -= 1;
                }
            }
        }
    }
}
