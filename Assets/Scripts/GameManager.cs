#define DEBUG_BAR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public BoardManager Board;
    public CameraManager cameraManager;

    public int gridRows;
    public int gridCols;

    private string inGameTestValue = "5";

    void Start()
    {
        // Show mouse
        Cursor.visible = true;

        // Get the board manager
        Board = BoardManager.Singleton;

        // Initialize the grid
        Board.InitializeHexGrid(gridRows, gridCols);

        // TESTING CODE
        Board.MarkCenterHex();

        // Calculate map dimensions and pass them to the camera
        // TODO: Access this more elegantly
        Vector3 topMapCorner = Board.HexGrid[gridCols - 1][gridRows - 2].HexCenter;
        
        Rect mapArea = new Rect(0, 0, topMapCorner.x, topMapCorner.z);
        cameraManager = Camera.main.GetComponentInParent<CameraManager>();
        cameraManager.SetMapArea(mapArea);

        cameraManager.MoveCameraToCenter();
    }

    void Update()
    {

    }

    void OnGUI()
    {
#if DEBUG_BAR
        // Make a group in the corner of the screen
        GUI.Box(new Rect(0, Screen.height - 200, 100, 200), "Tests");

        inGameTestValue = GUI.TextField(
            new Rect(10, Screen.height - 170, 80, 30),
            inGameTestValue);

        if (GUI.Button(new Rect(10, Screen.height - 120, 80, 30), "Run Test"))
        {
            Debug.Log("Running test...");
        }

        if (GUI.Button(new Rect(10, Screen.height - 80, 80, 30), "Randomize Hex"))
        {
            Debug.Log("Inserting obstacles...");
            Board.RandomizeGrid();
        }

        if (GUI.Button(new Rect(10, Screen.height - 40, 80, 30), "Reset"))
        {
            Debug.Log("Resetting grid...");
            Board.ResetGrid();
        }
#endif
    }
}
